package fr.omnilog.maref.service;

import fr.omnilog.maref.model.Project;
import fr.omnilog.maref.repository.ProjectRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProjectService {
    private final ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    public void save(Project project){
        projectRepository.save(project);
    }

    public List<Project> findByProjectSize(String projectSize) {
        //TODO : ?????
        List<Project> projects = projectRepository.findAll();

        if ("small".contentEquals(projectSize)) {
            projects = projects
                    .stream()
                    .filter(p ->{
                        LocalDate endDate = p.getEndDate() == null ? LocalDate.now() : p.getEndDate();
                        return p.getStartDate().isAfter(endDate.minusMonths(3));
                    })
                    .collect(Collectors.toList());
        } else if ("medium".contentEquals(projectSize)) {
            projects = projects
                    .stream()
                    .filter(p ->{
                        LocalDate endDate = p.getEndDate() == null ? LocalDate.now() : p.getEndDate();
                        return p.getStartDate().isBefore(endDate.minusDays(91)) && p.getStartDate().isAfter(endDate.minusDays(730));
                    })
                    .collect(Collectors.toList());

        } else if ("large".contentEquals(projectSize)) {
            projects = projects
                    .stream()
                    .filter(p ->{
                        LocalDate endDate = p.getEndDate() == null ? LocalDate.now() : p.getEndDate();
                        return p.getStartDate().isBefore(endDate.minusDays(730));
                    })
                    .collect(Collectors.toList());
        }
        return projects;
    }
}
